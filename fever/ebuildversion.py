#!/usr/bin/python
# -*- coding: utf8 -*-

from pkgcore.config import load_config
from pkgcore.util import repo_utils, packages as pkgutils
from pkgcore.restrictions import values, packages
from pkgcore.ebuild import atom_restricts as atom

config = load_config()

domain = config.get_default('domain')

repos = repo_utils.get_raw_repos(domain.repos)

def compareVersionsFor(package_name, latest_version):
    name_restr = values.StrExactMatch(str(package_name))
    pkg_restr = packages.PackageRestriction("package",name_restr)
    best_atom = None

    for repo in repos:
        for pkgs in pkgutils.groupby_pkg(
            repo.itermatch(pkg_restr, sorter=sorted)):
            pkgs = list(pkgs)
            package = max(pkgs)
            best_atom = package.versioned_atom

    if best_atom:
        version_matcher = atom.VersionMatch('>=', str(latest_version))
        is_latest = version_matcher.match(best_atom)
        return {'best_version' : best_atom.version,
                'best_full'    : best_atom.fullver,
                'is_latest'    : is_latest}
    else:
        return {}

