#!/usr/bin/python
# -*- coding: utf8 -*-

# fever - by Michał Bentkowski 2007
# licensed under GPL

# this file contains a few functions not direct related with fever,
# but fever can't work without them.

# I decided to split it because the main file would be too inlegible

import re
import tempfile
import sys
import urllib

def unit(number):
	"""Changes bytes unit to more legible one"""
	mp=1024
	prefixes="","Ki","Mi","Gi","Ti","Pi" # probably we don't need more ones :-)
	for (p, i) in zip(prefixes, map(lambda x: mp**x, xrange(len(prefixes)))):
		x=float(number)/i
		if x < mp or p == "Pi":
			return "%0.2f %sB" % (x,p)

def download(url,verbose=False):
	"""Downloads specified URL. Returns its content"""
	block_size=10240
	temp=tempfile.TemporaryFile()
	file=urllib.urlopen(url)
	try:
		filesize=" of "+unit(long(file.headers["content-length"]))
	except KeyError:
		filesize=""
	block=file.read(block_size)
	bytes=0
	while block != "":
		bytes+=len(block)
		temp.write(block)
		if verbose:
			sys.stdout.write("\rDownloading %s... %s%s" % (file.url[:40], unit(bytes), filesize))
			sys.stdout.flush()
		block=file.read(block_size)
	if verbose: print "\nDownloaded!"
	temp.seek(0)
	return temp.read()

def rpmvercmp(a, b):
	""" Comparing algorithm based on original rpmvercmp """
	a,b=map(re.split,("[\W_]+|(\d+)",)*2,(str(a),str(b))) # split versions strings according to rpm version
						# comparing algorithm
	a,b=map(lambda x: [elem for elem in x if elem not in (None, "")],[a,b]) # and remove empty
										# elements
	for elem_a, elem_b in zip(a,b):
		try:
			elem_a=int(elem_a)
		except ValueError:
			pass
		try:
			elem_b=int(elem_b)
		except ValueError:
			pass
		elem_a_type,elem_b_type=map(type,(elem_a,elem_b))
		if elem_a_type != elem_b_type:
			if elem_a_type == int:
				return 1
			else:
				return -1
		elif (elem_a_type, elem_b_type) == (int,)*2:
			compare=cmp(int(elem_a),int(elem_b))
			if compare != 0:
				return compare
		else:
			compare=cmp(elem_a, elem_b)
			if compare != 0:
				return compare
	return cmp(len(a),len(b))

def vercmpsort(List):
        """Sorting based on an rpmvercmp algorithm"""
        for indexes in range(len(List)-1, 0, -1):
                for index in range(indexes):
                        if rpmvercmp(List[index],List[index+1]) == 1:
                                List[index],List[index+1]=List[index+1],List[index]
        List.reverse()
        return List
