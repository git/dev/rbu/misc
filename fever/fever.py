#!/usr/bin/python
# -*- coding: utf8 -*-

# Fever - by Michał Bentkowski 2007
# licensed under GPL

# Adapted to Gentoo by Robert Buchholz <rbu@gentoo.org>

# now, import the other things we need

from commons import download, vercmpsort
import ebuildversion
import re
from xml.sax.saxutils import unescape

#set timeout TODO: should be set in config file
import socket
socket.setdefaulttimeout(45)

import sys

class Fever:
	def __init__(self, debug     = 0,
			listformat   = "\* (?P<name>.*?) (?P<regex>.*) (?P<url>.*?)\n",
			listlocation = "http://fedoraproject.org/wiki/PackageMaintainers/FEver"):
		if debug >= 1:
			print "Starting download of Package list"
		self.debug=debug
		# it's sane to download the site with a list of packages at once
		site = download(listlocation, debug)
		# and let's parse it!
		self.pkglist = {} # this var will keep all info of packages
		for package in re.finditer(listformat, site):
			#retrieve dictionary with name regex and url
			dict = package.groupdict()
			name = dict['name']
			# we don't need name in dict anymore
			del dict['name']
			# sometimes there are escaped chars like &lt; in regex
			# so we need to unescape them first
			dict['regex'] = unescape(dict['regex'])
			self.pkglist[name] = dict
			self.pkglist[name]['checked'] = False
			if debug >= 2:
				print "%s parsed. Data: %s" % (name,dict)
		if debug >= 1:
			print "Finished building package list"
				
	def getPkgSiteVersion(self, pkgname):
		# download a proper site and get a package version
		site = download(self.pkglist[pkgname]['url'], self.debug)
		regex = self.pkglist[pkgname]['regex']
		# find all versions and sort'em
		versions = vercmpsort(re.findall(regex, site))
		# we need only the newest one
		return versions[0]
	
	def getPkgList(self):
		return self.pkglist
	
	def getPkgInfo(self, pkgname):
		return self.pkglist[pkgname]
	
	def checkPackage(self, pkgname):
		# this is probably the most important function,
		# it checks if repo version is newer than the available one.
		# Returns 0 if versions match or koji version is newer than site version
		# (it may happen if there's something wrong with regex) and 1 if a package's
		# owner need to update his package, -1 - if error.
		if self.debug >= 1:
			print 'Checking %s ...' % pkgname
		try:
			sitever = self.getPkgSiteVersion(pkgname)
			if self.debug >= 2:
				print 'Latest on site: %s' % str(sitever)
			comparison_result = ebuildversion.compareVersionsFor(pkgname, sitever)
			kojiver = comparison_result['best_version']
			if self.debug >= 2:
				print 'Latest ebuild: %s' % str(kojiver)
			is_latest = comparison_result['is_latest']
			if self.debug >= 2:
				if is_latest:
					print 'Is latest'
				else:
					print 'Needs update'
		except KeyError:
			if self.debug >= 1:
				print "%s was not found in Gentoo." % (pkgname)
			self.pkglist[pkgname]['checked']=False
			return -1
		except:
			# if no version were found, we don't need this package anymore
			if self.debug >= 1:
				print "%s won't be checked. Error. %s" % (pkgname, str(sys.exc_info()[0]))
			self.pkglist[pkgname]['checked']=False
			return -1
		self.pkglist[pkgname]['kojiver']=kojiver
		self.pkglist[pkgname]['sitever']=sitever
		self.pkglist[pkgname]['checked']=True
		if self.debug >= 2:
			print ""
		if not is_latest: #if a newer version's available
			self.pkglist[pkgname]['uptodate']=False
			return 1
		else: # if isn't
			self.pkglist[pkgname]['uptodate']=True
			return 0
		
	def checkAllPackages(self):
		# simple loop to check all packages at once.
		for package in self.pkglist.keys():
			result=self.checkPackage(package)
				
	def isPackageUpToDate(self, pkgname):
		# just check if a given package is up to date
		return self.pkglist[pkgname]['uptodate']
	
	def listUpToDate(self):
		# list all up-to-date packages
		return [dict([(k,v)]) for k,v in self.pkglist.iteritems() if v['checked']==True and v['uptodate']==True]
	
	def listNotUpToDate(self):
		# list all not-up-to-date packages
		return [dict([(k,v)]) for k,v in self.pkglist.iteritems() if v['checked']==True and v['uptodate']==False]
